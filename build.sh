#!/bin/bash

mkdir -p public
rm -rf public/*

sassc src/style.scss public/style.css
for page in $(ls src/pages); do
    if [[ "$page" == "index.html" ]]; then
        cat src/head.html "src/pages/$page" src/footer.html > "public/$page"
    else
        p_dest="public/$(sed 's/\.html//g' <(printf $page))"
        mkdir -p $p_dest
        cat src/head.html "src/pages/$page" src/footer.html > "$p_dest/index.html"
    fi
done

cp -r img public/img
